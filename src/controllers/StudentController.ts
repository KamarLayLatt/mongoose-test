import { Request, Response } from 'express';
import Student from '../models/Student';

export const index = async (req: Request, res: Response) => {
    const students = await Student.find();
    res.json(students);
};

export const store = async (req: Request, res: Response) => {
    const student = new Student({name: "Zaw Lay", age: 25});
    await student.save();
    res.json(student);
};
