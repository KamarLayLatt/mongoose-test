import mongoose,{ model, Schema, Document} from 'mongoose';

interface IStudent extends Document{
    name: string;
    age: number;
};

const studentSchema = new Schema<IStudent>({
    name: { type: String, required: true },
    age: {type: Number, required: true}
});

const Student = mongoose.model<IStudent>('Student', studentSchema);

export default Student;