import express from "express";
import {index as StudentIndex, store as StudentStore} from '../controllers/StudentController'

const router = express.Router()


router.get('/students', StudentIndex);
router.get('/students/store', StudentStore);
  
export default router;