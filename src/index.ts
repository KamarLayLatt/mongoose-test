import express, { Request, Response , Application } from 'express';
import dotenv from 'dotenv';
import apiRoutes from './routes/api';
import mongoose from 'mongoose';


//For env File 
dotenv.config();

const app: Application = express();
const port = process.env.PORT || 8000;

mongoose.connect('mongodb://localhost:27017/school');


app.get('/', (req: Request, res: Response) => {
  res.send('Welcome to Express & TypeScript Server GOOD BOY');
});

app.use('/api', apiRoutes);


app.listen(port, () => {
  console.log(`Server is Fire at http://localhost:${port}`);
});







// interface IStudentMethods {
//     speak(): string;
//   }

// type StudentModel = Model<IStudent, {}, IStudentMethods>;

// const studentSchema = new Schema<IStudent,StudentModel,IStudentMethods>({
//     name: { type: String, required: true }
// });


// studentSchema.method('speak', function speak(){
//     const doing = 'I am doing '+ this.name;
//     console.log(doing);
// });

// const Student = model<IStudent, StudentModel>('Student', studentSchema);


// run().catch(err => console.log(err));


// async function run() {
//     await connect('mongodb://localhost:27017/school');
    
//     const student1 = new Student({name: "Mg Mg Kamar."});
//     console.log(student1.name);
//     student1.speak();
// }

